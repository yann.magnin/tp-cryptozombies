pragma solidity ^0.8.13;

import "./zombiefactory.sol";

interface KittyInterface {
    function getKitty(uint256 _id) external view returns (
        bool isGestating,
        bool isReady,
        uint256 cooldownIndex,
        uint256 nextActionAt,
        uint256 siringWithId,
        uint256 birthTime,
        uint256 matronId,
        uint256 sireId,
        uint256 generation,
        uint256 genes
    );
}

contract ZombieFeeding is ZombieFactory {

    //---
    // Decorators
    //---

    modifier ownerOf(uint256 _zombieId) {
        require(zombieToOwner[_zombieId] == msg.sender);
        _;
    }

    //---
    // Internals
    //---

    function _triggerCooldown(Zombie storage _zombie) internal {
         _zombie.readyTime = uint32(block.timestamp + cooldownTime);
    }

    function _isReady(Zombie storage _zombie) internal view returns (bool) {
        return bool(_zombie.readyTime <= block.timestamp);
    }

    //---
    // Public
    //---

    KittyInterface kittyContract;

    function setKittyContractAddress(address _address) public onlyOwner {
        kittyContract = KittyInterface(_address);
    }

    function feedAndMultiply(
        uint256 _zombieId,
        uint256 _targetDna,
        string memory _species
    ) internal ownerOf(_zombieId) {
        Zombie storage myZombie = zombies[_zombieId];
        require(_isReady(myZombie) == true);

        uint256 newDna = (myZombie.dna + (_targetDna % dnaModulus)) / 2;
        bytes32 hash0 = keccak256(abi.encodePacked(_species));
        bytes32 hash1 = keccak256(abi.encodePacked("kitty"));
        if (hash0 == hash1) {
            newDna = (newDna - (newDna % 100)) + 99;
        }

        _createZombie("NoName", newDna);
        _triggerCooldown(myZombie);
    }

    function feedOnKitty(uint256 _zombieId, uint256 _kittyId) public {
        uint256 kittyDna = 0;
        (,,,,,,,,,kittyDna) = kittyContract.getKitty(_kittyId);
        feedAndMultiply(_zombieId, kittyDna, "kitty");
    }
}
