pragma solidity ^0.8.13;

import './zombiehelper.sol';

contract ZombieAttack is ZombieHelper {

    //---
    // Config/const information
    //---

    uint256 randNonce = 0;
    uint256 attackVictoryProbability = 70;

    //---
    // Internals
    //---

    function randMod(uint256 _modulus) internal returns (uint256) {
        randNonce += 1;
        return uint256(
            keccak256(
                abi.encodePacked(
                    block.timestamp,
                    msg.sender,
                    randNonce
                )
            )
        ) % _modulus;
    }

    //---
    // Externals
    //---

    function attack(
        uint256 _zombieId,
        uint256 _targetId
    ) external  ownerOf(_zombieId) {
        Zombie storage myZombie = zombies[_zombieId];
        Zombie storage enemyZombie = zombies[_targetId];
        uint256 rand = randMod(100);
    }
}
