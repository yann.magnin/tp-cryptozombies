pragma solidity ^0.8.13;

import './ownable.sol';

contract ZombieFactory is Ownable {

    /* const / configuration information */
    uint256 dnaDigits = 16;
    uint256 dnaModulus = 10 ** dnaDigits;
    uint256 cooldownTime = 3600 * 24;

    /*
    ** gas analysis (<dym> + 64 bytes):
    ** - `name`             : "string" type is not sized but 32bytes-aligned
    ** - `dna`              : "uint256" take 32bytes
    ** - `level|readyTime`  : "uint32" can by packed to a 32bytes slot
    */
    struct Zombie {
        string name;
        uint256 dna;
        uint32 level;
        uint32 readyTime;
    }
    Zombie[] public zombies;

    /* relationship mapping */
    mapping (uint256 => address) public zombieToOwner;
    mapping (address => uint256) ownerZombieCount;

    //---
    // Internals
    //---

    function _generateRandomDna(
        string memory _str
    ) private view returns (
        uint256
    ) {
        return uint256(keccak256(abi.encodePacked(_str))) % dnaModulus;
    }

    function _createZombie(
        string memory _name,
        uint256 _dna
    ) internal {
        Zombie memory new_zombie = Zombie(
            _name,
            _dna,
            1,
            uint32(block.timestamp + cooldownTime)
        );
        zombies.push(new_zombie);
        zombieToOwner[zombies.length - 1] = msg.sender;
        ownerZombieCount[msg.sender] += 1;
        emit NewZombie(
            zombies.length - 1,
            new_zombie.name,
            new_zombie.dna
        );
    }

    //---
    // Public
    //---

    event NewZombie(uint256 zombieId, string name, uint dna);

    function createRandomZombie(string memory _name) public {
        require(ownerZombieCount[msg.sender] == 0);
        _createZombie(
            _name,
            _generateRandomDna(_name)
        );
    }
}
